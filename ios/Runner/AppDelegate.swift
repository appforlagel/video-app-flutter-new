import UIKit
import Flutter
import Firebase
import GoogleSignIn

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate, GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
       
    }
    
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
   
//    FirebaseApp.configure()
    GIDSignIn.sharedInstance().clientID = "158205059907-3juerh0lfst72cig3cm7f7t7oog43abn.apps.googleusercontent.com"
    GIDSignIn.sharedInstance().delegate = self
    GeneratedPluginRegistrant.register(with: self)
 
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
}

