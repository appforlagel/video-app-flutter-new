import 'dart:async';
import 'dart:io';
import 'dart:math';

import 'package:cloudinary_sdk/cloudinary_sdk.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:provider/provider.dart';
import 'package:settings_ui/settings_ui.dart';
import 'package:video_app/auth/models/User.dart';
import 'package:video_app/core/utils/ApiUrl.dart';
import 'package:video_app/profile/models/Interest.dart';
import 'package:video_app/profile/providers/UserProvider.dart';
import 'package:video_app/questions/api/QuestionApiService.dart';
import 'package:video_app/questions/ask/AudienceSelect.dart';
import 'package:video_app/questions/models/QuestionData.dart';
import 'package:video_app/questions/models/QuestionRequest.dart';
import 'package:video_app/videoQuestion/selectInterest.dart';

import 'design_course_app_theme.dart';

class QuestionVideo extends StatefulWidget {
  final String video;

  QuestionVideo(this.video);

  @override
  _QuestionVideoState createState() => _QuestionVideoState();
}

class _QuestionVideoState extends State<QuestionVideo>
    with TickerProviderStateMixin {
  bool routedPushed = false;
  TextEditingController questionField;
  QuestionData question;

  bool sendingVideo = false;
  AudienceType result = AudienceType.Public;
  Interest selectInterest = new Interest(label: " ");
  TabController _tabController;
  User _userLogged;
  Response response;
  bool lockInBackground = true;
  bool notificationsEnabled = true;
  String textToShow;
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      new FlutterLocalNotificationsPlugin();

  @override
  void initState() {
    _tabController = new TabController(vsync: this, length: 1);
    super.initState();
    questionField = TextEditingController();
    _audienceButton();
  }

  @override
  void dispose() {
    _tabController.dispose();
    questionField.dispose();
    super.dispose();
  }

  final _keyform = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    _userLogged = Provider.of<UserProvider>(context, listen: false).userLogged;
    return Scaffold(
        appBar: AppBar(
          elevation: 1,
          title: Text(
            'Post',
            textAlign: TextAlign.left,
            style: TextStyle(
              fontWeight: FontWeight.w900,
              fontSize: 17,
              color: DesignCourseAppTheme.darkerText,
            ),
          ),
          centerTitle: true,
        ),
        body: LoadingOverlay(
            color: Colors.black26,
            isLoading: sendingVideo,
            child: Stack(children: <Widget>[
              SettingsList(
                backgroundColor: Colors.white,
                sections: [
                  CustomSection(
                    child: Row(
                      children: <Widget>[
                        Flexible(
                          child: Form(
                            key: _keyform,
                            child: TextFormField(
                                autocorrect: true,
                                keyboardType: TextInputType.text,
                                maxLines: 5,
                                controller: questionField,
                                validator: (s) {
                                  if (s.isEmpty) return "Required*";
                                  return null;
                                },
                                decoration: InputDecoration(
                                  hintText: 'Type a question for your video...',
                                  hintStyle: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontSize: 14,
                                    color: Colors.grey,
                                  ),
                                  filled: true,
                                  fillColor: Colors.white,
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(12.0)),
                                    borderSide: BorderSide(
                                        color: Colors.transparent, width: 2),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10.0)),
                                    borderSide: BorderSide(
                                        color: Colors.white, width: 2),
                                  ),
                                )),
                          ),
                        ),
                      ],
                    ),
                  ),
                  CustomSection(
                    child: const Divider(
                      color: Colors.grey,
                      height: 19,
                      thickness: 0.4,
                      indent: 15,
                      endIndent: 15,
                    ),
                  ),
                  CustomSection(
                    child: InkWell(
                      child: Container(
                          margin: new EdgeInsets.only(
                              top: 10, left: 13, bottom: 10, right: 10),
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Icon(
                                        Icons.lock_open_outlined,
                                        color: Colors.grey,
                                        size: 20,
                                      ),
                                      SizedBox(width: 10),
                                      Text(
                                        'Public or Anonymous Question',
                                        style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 16,
                                          letterSpacing: 0,
                                          wordSpacing: 0,
                                          color: Colors.black87,
                                        ),
                                      ),
                                    ]),
                                Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      Text(
                                        textToShow.toString(),
                                        style: TextStyle(
                                          fontWeight: FontWeight.w600,
                                          fontSize: 14,
                                          letterSpacing: 0,
                                          wordSpacing: 0,
                                          color: Colors.grey,
                                        ),
                                      ),
                                      Icon(Icons.arrow_forward_ios,
                                          color: Colors.grey, size: 14)
                                    ]),
                              ])),
                      onTap: () async {
                        if (result != AudienceType.Public) {
                          result = AudienceType.Anonymous;
                        } else if (result != AudienceType.Anonymous) {
                          result = AudienceType.Public;
                        }

                        result = await Navigator.push(context,
                            MaterialPageRoute(builder: (BuildContext context) {
                          return AudienceSelect(
                            defaultAudience: result,
                          );
                        }));
                        _audienceButton();
                        print(result.toString() +
                            "_audienceSelected" +
                            result.toString());
                        if (result != AudienceType.Anonymous) {
                          result = AudienceType.Public;
                        } else if (result != AudienceType.Public) {
                          result = AudienceType.Anonymous;
                        }
                      },
                    ),
                  ),
                  CustomSection(
                    child: const Divider(
                      color: Colors.grey,
                      height: 19,
                      thickness: 0.1,
                      indent: 15,
                      endIndent: 15,
                    ),
                  ),
                  CustomSection(
                    child: InkWell(
                      child: Container(
                          margin: new EdgeInsets.only(
                              top: 10, left: 13, bottom: 10, right: 10),
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Icon(
                                        Icons.library_add_check,
                                        color: Colors.grey,
                                        size: 20,
                                      ),
                                      SizedBox(width: 10),
                                      Text(
                                        'Select your topic',
                                        style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 16,
                                          letterSpacing: 0,
                                          wordSpacing: 0,
                                          color: Colors.black87,
                                        ),
                                      ),
                                      Padding(
                                          padding: const EdgeInsets.only(
                                              top: 0,
                                              bottom: 8,
                                              left: 0,
                                              right: 0),
                                          child: Icon(
                                            Icons.star_rounded,
                                            color: Colors.grey,
                                            size: 10,
                                          )),
                                    ]),
                                Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      text(),
                                      Icon(
                                        Icons.arrow_forward_ios,
                                        color: Colors.grey,
                                        size: 14,
                                      )
                                    ]),
                              ])),
                      onTap: () async {
                        selectInterest = await Navigator.push(
                            context,
                            new MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  new SelectCategory(),
                              fullscreenDialog: true,
                            ));
                      },
                    ),
                  ),
                  CustomSection(
                    child: Row(
                      children: [
                        SizedBox(
                          height: 100,
                        )
                      ],
                    ),
                  ),
                  CustomSection(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        SizedBox(
                          height: 150,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            showProgress
                                ? Container(
                                    width: 20,
                                    height: 20,
                                    child: Center(
                                        child: CircularProgressIndicator(
                                            strokeWidth: .5)))
                                : RaisedButton.icon(
                                    padding: const EdgeInsets.only(
                                        top: 10,
                                        bottom: 8,
                                        left: 50,
                                        right: 50),
                                    onPressed: () {
                                      if (_keyform.currentState.validate()) {
                                        _sendQuestion();
                                      }
                                    },
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(20))),
                                    label: Text(
                                      'POST',
                                      style: TextStyle(color: Colors.white),
                                    ),
                                    icon: Icon(
                                      Icons.unarchive_rounded,
                                      color: Colors.white,
                                    ),
                                    textColor: Colors.white,
                                    splashColor: Colors.blueAccent,
                                    color: Colors.lightBlue,
                                  ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ])));
  }

  bool showProgress = false;
  _sendQuestion() async {
    setState(() {
      showProgress = true;
    });
    QuestionRequest _request = QuestionRequest();
    _request.type = QuestionType.GENERAL_QUESTION;
    _request.interestId = selectInterest.id;

    var question = QuestionData(text: questionField.text, privacy: result);
    _request.userSenderId = _userLogged.id;
    _request.questionData = question;

    try {
      print(selectInterest.label.length);
      if (selectInterest.label.length != 1) {
        Response response = await QuestionApiService().sendQuestion(_request);
        if (response.statusCode == 200) {
          print("id is " + response.data);
          _uploadAndSaveVideo(response.data);
          // Navigator.of(context).popUntil((_) => count++ >= 2);
          //Navigator.of(context).popUntil((route) => route.isFirst);
        } else if (response.statusCode == 422) {
          //Navigator.of(context).popUntil((route) => route.isFirst);
        } else if (response.statusCode == 404) {
          //Navigator.of(context).popUntil((route) => route.isFirst);
        }
      } else {
        setState(() {
          showProgress = false;
        });
        print("false");
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: new Text("Required"),
              content:
                  new Text("Please write a description and select a topic"),
              actions: <Widget>[
                new TextButton(
                  child: new Text("ok"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      }
    } catch (e) {
      setState(() {
        showProgress = false;
      });
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: new Text("Required"),
            content: new Text("Please write a description and select a topic"),
            actions: <Widget>[
              new TextButton(
                child: new Text("ok"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }
  }

  Future<void> _uploadAndSaveVideo(id) async {
    setState(() {
      sendingVideo = true;
    });

    var rng = new Random();
    _showProgressNotification(rng.nextInt(100));

    // String compressedVideo = (await _compressVideo(widget.video)).path;
    // CloudinaryClient client = new CloudinaryClient(ApiUrl.CLOUDINARY_KEY,
    // ApiUrl.CLOUDINARY_SECRET, ApiUrl.CLODINARY_CLOUD_NAME);

    final Cloudinary client = Cloudinary(ApiUrl.CLOUDINARY_KEY,
        ApiUrl.CLOUDINARY_SECRET, ApiUrl.CLODINARY_CLOUD_NAME);

    try {
      print('Before Navigation');
       Navigator.pushNamedAndRemoveUntil(
          context, 'feed', (route) => false);
      // await Navigator.pushNamed(context, 'feed');

      print('Uploading ...');
      assert(widget.video != null && File(widget.video).existsSync());
      // CloudinaryResponse result = await client.uploadFile(
      //   filePath: widget.video,
      //   fileBytes: File(widget.video).readAsBytesSync().toList(),
      //   fileName: 'question',
      //   folder: _userLogged.email.split('@')[0],
      //   resourceType: CloudinaryResourceType.video,
      // );

      CloudinaryResponse result =
          await client.uploadResource(CloudinaryUploadResource(
        filePath: widget.video,
        fileBytes: File(widget.video).readAsBytesSync().toList(),
        resourceType: CloudinaryResourceType.video,
        folder: _userLogged.email.split('@')[0],
        fileName: 'question',
      ));
      this.response = await QuestionApiService().answerQuestion({
        'questionId': id,
        'answer': {
          'video': result.secureUrl,
          'cloudinaryPublicId': result.publicId
        },
        'userId': _userLogged.id,
      });
    } catch (e) {
      print(e);
    }
  }

  Future<void> _showProgressNotification(int id) async {
    const int maxProgress = 10;
    for (int i = 0; i <= maxProgress; i++) {
      await Future<void>.delayed(const Duration(seconds: 1), () async {
        final AndroidNotificationDetails androidPlatformChannelSpecifics =
            AndroidNotificationDetails(
                "Channel", 'progress channel', 'progress channel description',
                channelShowBadge: false,
                importance: Importance.max,
                priority: Priority.high,
                onlyAlertOnce: true,
                showProgress: true,
                maxProgress: maxProgress,
                progress: i);
        final NotificationDetails platformChannelSpecifics =
            NotificationDetails(android: androidPlatformChannelSpecifics);
        await flutterLocalNotificationsPlugin.show(
            id, 'Uploading', 'Answer', platformChannelSpecifics,
            payload: 'item x');
      });

      if (i == maxProgress) {
        await Future<void>.delayed(const Duration(seconds: 1), () async {
          final AndroidNotificationDetails androidPlatformChannelSpecifics =
              AndroidNotificationDetails(
                  'Channel', 'progress channel', 'progress channel description',
                  channelShowBadge: false,
                  importance: Importance.max,
                  priority: Priority.high,
                  onlyAlertOnce: true,
                  showProgress: false,
                  maxProgress: 0,
                  progress: 0);
          final NotificationDetails platformChannelSpecifics =
              NotificationDetails(android: androidPlatformChannelSpecifics);
          await flutterLocalNotificationsPlugin.show(
              id, 'Uploaded successfully', 'Answer', platformChannelSpecifics,
              payload: 'item x');
        });
      }
    }
  }

  text() {
    try {
      return Text(
        selectInterest.label != null ? selectInterest.label : "",
        style: TextStyle(
          fontWeight: FontWeight.w600,
          fontSize: 14,
          letterSpacing: 0,
          wordSpacing: 0,
          color: Colors.grey,
        ),
      );
    } catch (e) {
      selectInterest = new Interest(id: " ", label: " ", icon: " ");
    }
  }

  _audienceButton() {
    switch (result) {
      case AudienceType.Public:
        textToShow = 'Public';
        break;
      case AudienceType.Anonymous:
        textToShow = 'Anonymous';
        break;
      case AudienceType.Limited:
        textToShow = 'Limited';
        break;
    }
  }
}
